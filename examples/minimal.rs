//! This state machine switches from `StateA` to `StateB` on a single event but
//! then needs three events to switch back to `StateA`. Additionally it keeps
//! track of how often it got toggled back from `StateB` to `StateA`.

use apparat::prelude::*;

// Define the necessary types
// --------------------------

// States

#[derive(Debug, Default)]
pub struct StateA;

#[derive(Debug, Default)]
pub struct StateB {
    ignored_events: usize,
}

// Context

// Data that survives state transitions and can be accessed in all states
#[derive(Debug, Default)]
pub struct ContextData {
    toggled: usize,
}

// Auto-generate the state wrapper and auto-implement traits
// ---------------------------------------------------------

// In this example we are just using the unit type for `event` and `output`
// because we are only handling one kind of event and we don't care about values
// being returned when events are handled.
build_wrapper! {
    states: [StateA, StateB],
    wrapper: MyStateWrapper, // This is just an identifier we can pick
    context: ContextData,
    event: (),
    output: (),
}

// Define transitions
// ------------------

impl TransitionFrom<StateB> for StateA {
    fn transition_from(_prev: StateB, ctx: &mut ContextData) -> Self {
        // Increase toggled value
        ctx.toggled += 1;
        println!("B -> A          | toggled: {}", ctx.toggled);
        StateA
    }
}

impl TransitionFrom<StateA> for StateB {
    fn transition_from(_prev: StateA, ctx: &mut ContextData) -> Self {
        println!("A -> B          | toggled: {}", ctx.toggled);
        StateB::default()
    }
}

// Implement the `ApparatState` trait for all states
// -------------------------------------------------

impl ApparatState for StateA {
    type Wrapper = MyStateWrapper;

    fn handle(self, _event: (), ctx: &mut ContextData) -> Handled<MyStateWrapper> {
        println!("A handles event | toggled: {}", ctx.toggled);
        // Transition to `StateB`
        let state_b = self.transition::<StateB>(ctx);
        // Now we need to wrap that `state_b` in a `MyStateWrapper`...
        let state_b_wrapped = state_b.wrap();
        // ... and add an output value to turn it into a `Handled<...>`.
        state_b_wrapped.default_output()
        // If we would need a different output value or our output type wouldn't
        // implement `Default` we would have to use `.with_output(...)` instead.
    }
}

impl ApparatState for StateB {
    type Wrapper = MyStateWrapper;

    fn handle(mut self, _event: (), ctx: &mut ContextData) -> Handled<MyStateWrapper> {
        println!("B handles event | toggled: {}", ctx.toggled);
        if self.ignored_events == 2 {
            self.transition::<StateA>(ctx).wrap().default_output()
        } else {
            self.ignored_events += 1;
            self.wrap().default_output()
        }
    }
}

// Run the machine
// ---------------

fn main() {
    let mut apparat = Apparat::new(StateA.wrap(), ContextData::default());

    // Handle some events
    for _ in 0..10 {
        apparat.handle(());
    }
}
