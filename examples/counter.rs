//! A counter that counts until 9 and tracks how often it has reached 9.

use apparat::prelude::*;

// States (arbitrary structs or enums that implement `Debug`)
// These are allowed to hold their own data which is only accessible in
// that particular state and will be lost when transitioning to other states.
// Shared data should be stored in the context type (see further below).

#[derive(Debug, Default)]
pub struct Starting;

#[derive(Debug, Default)]
pub struct Counting {
    count: usize,
}

#[derive(Debug)]
pub struct Finished;

/// The event type that all states can handle
#[derive(Debug, Clone)]
pub enum CounterEvent {
    Advance,
    Restart,
}

/// A type that holds data that can be accessed and mutated from all states
#[derive(Debug, Default)]
pub struct CounterContext {
    finished: usize,
}

/// A type that is returned when events are handled, indicating if they got
/// ignored or not
#[derive(Debug)]
pub enum CounterOutput {
    Executed,
    Ignored,
}

impl Default for CounterOutput {
    fn default() -> Self {
        Self::Executed
    }
}

// Transitions

impl TransitionFrom<Starting> for Counting {
    fn transition_from(_prev: Starting, _ctx: &mut CounterContext) -> Self {
        Counting::default()
    }
}

impl TransitionFrom<Counting> for Finished {
    fn transition_from(_prev: Counting, ctx: &mut CounterContext) -> Self {
        ctx.finished += 1;
        Finished
    }
}

impl TransitionFrom<Counting> for Starting {
    fn transition_from(_prev: Counting, _ctx: &mut CounterContext) -> Self {
        Starting
    }
}

impl TransitionFrom<Finished> for Starting {
    fn transition_from(_prev: Finished, _ctx: &mut CounterContext) -> Self {
        Starting
    }
}

// Event handling and initialization of states

impl ApparatState for Starting {
    type Wrapper = CounterWrapper;

    fn init(self, ctx: &mut CounterContext) -> CounterWrapper {
        println!("transitioning from starting to counting state");
        self.transition::<Counting>(ctx).wrap()
    }

    fn handle(self, _event: CounterEvent, _ctx: &mut CounterContext) -> Handled<CounterWrapper> {
        // This is unreachable because whenever this state occurs within our
        // `Apparat`, it will change to a different state before any event can
        // be handled. In this example this `Starting` state is pretty useless,
        // but it demonstrates the initialization mechanism.
        unreachable!()
    }

    fn is_init(&self, _ctx: &CounterContext) -> bool {
        false
    }
}

impl ApparatState for Counting {
    type Wrapper = CounterWrapper;

    fn handle(mut self, event: CounterEvent, ctx: &mut CounterContext) -> Handled<CounterWrapper> {
        println!("handling event in counting state (count: {})", self.count);
        match event {
            CounterEvent::Advance => {
                self.count += 1;
                if self.count < 10 {
                    self.wrap().default_output()
                } else {
                    println!("transitioning from counting to finished state");
                    self.transition::<Finished>(ctx).wrap().default_output()
                }
            }
            CounterEvent::Restart => {
                println!("restarting");
                self.transition::<Starting>(ctx).wrap().default_output()
            }
        }
    }
}

impl ApparatState for Finished {
    type Wrapper = CounterWrapper;

    fn handle(self, event: CounterEvent, ctx: &mut CounterContext) -> Handled<CounterWrapper> {
        match event {
            CounterEvent::Advance => {
                println!("ignoring advance event in finished state");
                self.wrap().with_output(CounterOutput::Ignored)
            }
            CounterEvent::Restart => {
                println!("restarting");
                self.transition::<Starting>(ctx).wrap().default_output()
            }
        }
    }
}

// Pass your custom types into this macro to auto generate all the necessary
// boilerplate for the state machine to work
build_wrapper! {
    states: [Starting, Counting, Finished],
    wrapper: CounterWrapper,
    context: CounterContext,
    event: CounterEvent,
    output: CounterOutput,
}

fn main() {
    let mut counter = Apparat::new(Starting.wrap(), CounterContext::default());

    // Handle a single event (ignoring the output)
    counter.handle(CounterEvent::Advance);

    // Handle all events from an iterator (could be the receiving end of a channel)
    use std::iter::{once, repeat};
    for event in repeat(CounterEvent::Advance)
        .take(5)
        .chain(once(CounterEvent::Restart))
        .chain(repeat(CounterEvent::Advance).take(10))
        .chain(once(CounterEvent::Advance))
    {
        let output = counter.handle(event);
        println!("Output from handling event: {output:?}");
        // The output of the last event will be `CounterOutput::Ignored` because
        // the counter is already finished and can't be advanced at that point.
    }

    println!("\nFinished state: {counter:#?}\n");

    // Restart the counter again. The `finished` value is state
    // independent and stays at `1`.
    counter.handle(CounterEvent::Restart);

    println!("\nFinal state: {counter:#?}");
}
