#![doc = include_str!("../readme.md")]
#![no_std]

pub mod prelude {
    pub use crate::{
        build_wrapper, transitions, Apparat, ApparatState, ApparatTrait, ApparatWrapper,
        ApparatWrapperDefaultOutput, Handled, TransitionFrom, TransitionTo, Wrap,
    };
}

/// This trait is used to associate all the types used together in an `Apparat`
/// with the state wrapper enum, so users of the library dont' need to specify
/// all these types every time they implement the `ApparatState` trait for one
/// of their state types.
pub trait ApparatWrapper: Sized {
    type Context;
    type Event;
    type Output;

    fn with_output(self, output: Self::Output) -> Handled<Self> {
        Handled::new(self, output)
    }
}

pub trait ApparatWrapperDefaultOutput: ApparatWrapper {
    fn default_output(self) -> Handled<Self>;
}

impl<Wrapper: ApparatWrapper> ApparatWrapperDefaultOutput for Wrapper
where
    Wrapper::Output: Default,
{
    #[inline]
    fn default_output(self) -> Handled<Self> {
        self.into()
    }
}

pub trait ApparatTrait<Wrapper>
where
    Wrapper: ApparatWrapper + ApparatState<Wrapper = Wrapper>,
{
    fn new(state: Wrapper, ctx: Wrapper::Context) -> Self;
    fn handle(&mut self, event: Wrapper::Event) -> Wrapper::Output;
}

/// The actual state machine that handles your events and manages their
/// initialization and transitions
#[derive(Debug)]
pub struct Apparat<Wrapper: ApparatWrapper> {
    state: Option<Wrapper>,
    ctx: Wrapper::Context,
}

impl<Wrapper> ApparatTrait<Wrapper> for Apparat<Wrapper>
where
    Wrapper: ApparatWrapper + ApparatState<Wrapper = Wrapper>,
{
    fn new(mut state: Wrapper, mut ctx: Wrapper::Context) -> Self {
        while !state.is_init(&ctx) {
            state = state.init(&mut ctx);
        }
        Self {
            state: Some(state),
            ctx,
        }
    }

    #[inline]
    fn handle(&mut self, event: Wrapper::Event) -> Wrapper::Output {
        #[cfg(feature = "unsafe")]
        // Safety: `self.state` can't be accessed directly from outside of this
        // module. The only methods accessing it are this one and `new`. Both
        // ultimately leave a `Some` value in `self.state`. Panicking while it's
        // `None` would only lead to undefined behavior if there would be an
        // unsafe `Drop` implementation for `Apparat` that assumes that it's
        // `Some`, which isn't the case. Since this method takes `&mut self`
        // there also can't be an overlapping call from a different thread
        // thanks to the almighty borrow checker <3.
        let state = self
            .state
            .take()
            .unwrap_or_else(|| unsafe { core::hint::unreachable_unchecked() });

        #[cfg(not(feature = "unsafe"))]
        // The performance impact is negligible in most cases, but using this
        // "safe" version does sometimes lead to slightly less optimized code.
        let state = self.state.take().expect("https://xkcd.com/2200/");

        let mut handled = state.handle(event, &mut self.ctx);
        while !handled.state.is_init(&self.ctx) {
            handled.state = handled.state.init(&mut self.ctx);
        }
        self.state = Some(handled.state);
        handled.result
    }
}

/// A trait that must be implemented by all provided state types. Have a look at
/// the readme or the examples for details.
pub trait ApparatState: Sized {
    type Wrapper: ApparatWrapper + From<Self>;

    #[inline(always)]
    fn init(
        self,
        _ctx: &mut <<Self as ApparatState>::Wrapper as ApparatWrapper>::Context,
    ) -> Self::Wrapper {
        Self::Wrapper::from(self)
    }

    fn handle(
        self,
        _event: <<Self as ApparatState>::Wrapper as ApparatWrapper>::Event,
        _ctx: &mut <<Self as ApparatState>::Wrapper as ApparatWrapper>::Context,
    ) -> Handled<<Self as ApparatState>::Wrapper>;

    #[inline(always)]
    fn is_init(&self, _ctx: &<<Self as ApparatState>::Wrapper as ApparatWrapper>::Context) -> bool {
        true
    }
}

/// This type is being returned whenever an event is handled by a state type. It
/// contains the new state alongside an output value that will be returned  to
/// the caller of the `handle` method.
#[derive(Debug)]
pub struct Handled<Wrapper: ApparatWrapper> {
    pub state: Wrapper,
    pub result: Wrapper::Output,
}

impl<Wrapper: ApparatWrapper> Handled<Wrapper> {
    #[inline]
    pub fn new(state: Wrapper, result: Wrapper::Output) -> Self {
        Self { state, result }
    }
}

impl<Wrapper> Handled<Wrapper>
where
    Wrapper: ApparatWrapper,
    Wrapper::Output: Default,
{
    #[inline]
    pub fn new_default(state: Wrapper) -> Self {
        Self {
            state,
            result: Wrapper::Output::default(),
        }
    }
}

impl<Wrapper> From<Wrapper> for Handled<Wrapper>
where
    Wrapper: ApparatWrapper,
    Wrapper::Output: Default,
{
    #[inline(always)]
    fn from(wrapper: Wrapper) -> Self {
        Self::new_default(wrapper)
    }
}

/// Define transitions between states. These transition functions can access the
/// shared context/data mutably.
pub trait TransitionFrom<FromState: ApparatState> {
    fn transition_from(
        prev: FromState,
        _ctx: &mut <<FromState as ApparatState>::Wrapper as ApparatWrapper>::Context,
    ) -> Self;
}

/// Similar to `Into` in std, this is mostly for convenience and should not get
/// implemented manually. Implement `TransitionFrom` instead and use
/// `TransitionInto` in trait bounds, when needed.
pub trait TransitionTo<Context>: Sized + ApparatState {
    #[inline(always)]
    fn transition<Next>(
        self,
        ctx: &mut <<Self as ApparatState>::Wrapper as ApparatWrapper>::Context,
    ) -> Next
    where
        Next: ApparatState<Wrapper = Self::Wrapper> + TransitionFrom<Self>,
    {
        Next::transition_from(self, ctx)
    }
}

// Blanket implementation so state types can transition into themselves
impl<T: ApparatState> TransitionTo<T> for T {}

/// An alternative to `std::Into` for turning state types into the respective
/// state wrapper enum. This is preferred over `Into` because it provides more
/// reliable type inference in the context of apparat.
pub trait Wrap<Wrapper> {
    fn wrap(self) -> Wrapper;
}

/// Generate an enum that wraps all provided state types. Additionally all
/// necessary traits are implemented for it, so the wrapper can be used within
/// an `Apparat` state machine.
#[macro_export]
macro_rules! build_wrapper {
    (
        states: [$state1:ident, $($state:ident),* $(,)*],
        wrapper: $wrapper:ident,
        context: $context:ty,
        event: $event:ty,
        output: $output:ty
        $(,)*
    ) => {
        #[derive(Debug)]
        #[allow(clippy::enum_variant_names)]
        pub enum $wrapper {
            $state1($state1),
            $(
                $state($state)
            ),*
        }

        impl ::apparat::ApparatWrapper for $wrapper {
            type Context = $context;
            type Event = $event;
            type Output = $output;
        }

        impl ::apparat::ApparatState for $wrapper {
            type Wrapper = Self;

            #[inline]
            fn init(self, ctx: &mut $context) -> Self {
                match self {
                    $wrapper::$state1(state) => state.init(ctx),
                    $(
                        $wrapper::$state(state) => state.init(ctx)
                    ),*
                }
            }

            #[inline]
            fn handle(self, event: $event, ctx: &mut $context) -> Handled<$wrapper> {
                match self {
                    $wrapper::$state1(state) => state.handle(event, ctx),
                    $(
                        $wrapper::$state(state) => state.handle(event, ctx)
                    ),*
                }
            }

            #[inline]
            fn is_init(&self, ctx: &$context) -> bool {
                match self {
                    $wrapper::$state1(state) => state.is_init(ctx),
                    $(
                        $wrapper::$state(state) => state.is_init(ctx)
                    ),*
                }
            }
        }

        impl ::std::convert::From<$state1> for $wrapper {
            #[inline(always)]
            fn from(state: $state1) -> $wrapper {
                $wrapper::$state1(state)
            }
        }

        // These manual implementations are needed (instead of a blanket impl) so
        // the wrapper type can get inferred when wrap is called on a state
        // object.
        impl ::apparat::Wrap<$wrapper> for $state1 {
            #[inline(always)]
            fn wrap(self) -> $wrapper {
                $wrapper::$state1(self)
            }
        }

        $(
            impl ::std::convert::From<$state> for $wrapper {
                #[inline(always)]
                fn from(state: $state) -> $wrapper {
                    $wrapper::$state(state)
                }
            }

            impl ::apparat::Wrap<$wrapper> for $state {
                #[inline(always)]
                fn wrap(self) -> $wrapper {
                    $wrapper::$state(self)
                }
            }
        )*
    };
}

/// Short hand for implementing the `TransitionFrom` trait for multiple state
/// types, defining multiple transitions. This trait can easily be implemented
/// manually too, so this macro is just an optional tool for convenience. Find a
/// usage demo in the example `counter_transitions_macro`.
#[macro_export]
macro_rules! transitions {
    ($($state_a:ident -> $state_b:ident :: $function:ident),* $(,)*) => {
        $(
            impl TransitionFrom<$state_a> for $state_b {
                fn transition_from(
                    prev: $state_a,
                    ctx: &mut <<$state_a as ApparatState>::Wrapper as ApparatWrapper>::Context
                ) -> Self {
                    $state_b::$function(prev, ctx)
                }
            }
        )*
    };
}
